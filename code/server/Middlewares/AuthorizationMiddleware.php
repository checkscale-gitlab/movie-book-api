<?php

namespace App\Server\Middlewares;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Server\Classes;

class AuthorizationMiddleware
{
    static function authenticate(Request $request, Response $response, $next)
    {
        // Get authorization header and check it isn't empty
        $authStr = $request->getHeader('Authorization')[0];
        if (empty($authStr)) {
            return $response->withStatus(401);
        }

        // Split header to fetch auth type and token. Authorization: <type> <credentials>
        $auth = explode(' ', $authStr);
        if (count($auth) !== 2 || $auth[0] !== 'Bearer' || empty($auth[1])) {
            return $response->withStatus(401, 'Invalid authorization header');
        }

        // Decode the JWT, validation errors will throw exceptions
        try {
            $jwt = Classes\JWT::validate($auth[1]);
        } catch (\Exception $e) {
            return $response->withStatus(401, $e->getMessage());
        }

        // Check JWT scope to allow/deny access to API
        if (!in_array('api_access', $jwt->scopes)) {
            return $response->withStatus(403);
        }

        // Pass request to next handler
        return $next($request, $response);
    }
}