<?php

namespace App\Server\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Server\Classes;

class MovieController
{
    private static $client;

    static function init(){
        self::$client = new Classes\OMDBClient();
    }

    static function getMovies (Request $request, Response $response, array $args) {
        $title = $request->getQueryParam('title');
        $year = $request->getQueryParam('year');
        $plot = $request->getQueryParam('plot');

        $res = self::$client->getMovie($title, $year, $plot);

        return $response->withJson($res);
    }
}

// Auto-initialize class when loaded
MovieController::init();