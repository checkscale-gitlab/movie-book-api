<?php

namespace App\Server\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Server\Classes;

class BookController
{
    private static $client;

    static function init(){
        self::$client = new Classes\LibraryClient();
    }

    static function getBooks (Request $request, Response $response, array $args) {
        $isbn = $args['isbn'];
        $res = self::$client->getBook($isbn);
        return $response->withJson($res);
    }
}

// Auto-initialize class when loaded
BookController::init();