<?php

namespace App\Server\Controllers;

use App\Server\Middlewares;
use Slim\App;

class RouteController
{
    static function registerRoutes(App $app)
    {
        // Register API endpoints as a group with authentication middleware
        $app->group('/api', function () use ($app) {

            $app->get('/getMovie', MovieController::class . ':getMovies');
            $app->get('/getBook/{isbn}', BookController::class . ':getBooks');

        })->add(Middlewares\AuthorizationMiddleware::class . ':authenticate');

        // Add login endpoint outside authenticated group
        $app->post('/login', LoginController::class . ':login');
    }
}