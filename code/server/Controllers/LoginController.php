<?php

namespace App\Server\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Server\Classes;

class LoginController
{
    static function login (Request $request, Response $response, array $args) {
        $requestBody = $request->getParsedBody();
        $username = $requestBody['username'];
        $password = $requestBody['password'];
        $redirectUrl = $requestBody['redirectTo'];

        if (!Classes\MockLogin::checkCredentials($username, $password)) {
            return $response->withStatus(401, 'Wrong username or password');
        }

        $jwt = new Classes\JWT();

        return $response
            ->withStatus(302)
            ->withHeader('Location', "$redirectUrl?token=$jwt");
    }
}