<?php

namespace App\Server\Classes;

class MockLogin
{
    private static $hash;

    static function init()
    {
        self::$hash = getenv('ADMIN_HASH');
    }

    static function checkCredentials($username, $password)
    {
        if ($username === 'admin' && password_verify($password, self::$hash)) {
            return true;
        }
        return false;
    }
}

// Auto-initialize class when loaded
MockLogin::init();