<?php

namespace App\Server\Classes;

use App\Shared\HTTPClient;

class LibraryClient extends HTTPClient
{
    function __construct()
    {
        parent::__construct('http://openlibrary.org/api');
    }

    function getBook($isbn)
    {
        return $this->get('/books', [
            // Format response as json
            'format'  => 'json',

            // According to the API documentation, this option returns the most data about the book
            'jscmd'   => 'data',
            'bibkeys' => "ISBN:$isbn",
        ]);
    }
}
