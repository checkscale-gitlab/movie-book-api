<?php

namespace App\Server\Classes;

use App\Shared\HTTPClient;

class OMDBClient extends HTTPClient
{
    private $apiKey;

    function __construct()
    {
        parent::__construct('http://www.omdbapi.com');
        $this->apiKey = getenv('OMDB_API_KEY');
    }

    function getMovie($title, $year = null, $plot = null)
    {
        return $this->get('/', [
            'apiKey' => $this->apiKey,
            't'      => $title,
            'y'      => $year,
            'plot'   => $plot,
        ]);
    }
}
