<?php

namespace App\Server;

require '/code/vendor/autoload.php';

use App\Server\Controllers\RouteController;
use Slim\App;

// Create Slim app
$app = new App;

// Register routes outside index.php to keep it clean
RouteController::registerRoutes($app);

// Run the app
$app->run();
