<?php

namespace App\Shared;

/*
 * HTTP client for making outbound HTTP requests
 *
 * Provides a base to extend other specific clients
 */

class HTTPClient
{
    private $baseUrl, $headers;

    function __construct($baseUrl, $headers = [])
    {
        $this->baseUrl = $baseUrl;
        $this->headers = $headers;
    }

    protected function post($endpoint, $bodyObject)
    {
        $body = json_encode($bodyObject);
        return $this->request('POST', $endpoint, $body);
    }

    protected function get($endpoint, $urlParams = [])
    {
        $urlString = '';
        $i = 0;
        foreach ($urlParams as $key => $value) {
            if ($value === null) {
                continue;
            }
            if ($i == 0) {
                $urlString .= "?$key=$value";
            } else {
                $urlString .= "&$key=$value";
            }
            $i++;
        }
        return $this->request('GET', $endpoint . $urlString);
    }

    private function request($method, $endpoint, $body = null)
    {
        $url = $this->baseUrl . $endpoint;
        $ch = curl_init();
        $headers = array_merge($this->headers, [
            'Accept: application/json',
            'Content-Type: application/json',
        ]);
        curl_setopt_array($ch, [
            CURLOPT_URL            => $url,
            CURLOPT_HTTPHEADER     => $headers,
            CURLOPT_HEADER         => 0,
            CURLOPT_CUSTOMREQUEST  => $method,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POSTFIELDS     => $body,
            CURLOPT_TIMEOUT        => 30,
            CURLOPT_VERBOSE        => true,
        ]);

        $result = curl_exec($ch);

        return $this->response($result);
    }

    protected function response($result)
    {
        $response = (object)[
            'success'      => false,
            'error'        => -1,
            'errorMessage' => 'Unknown error',
            'data'         => null,
        ];

        if ($result !== false) {
            $jsonResponse = json_decode($result);
            $jsonErr = json_last_error();
            if ($jsonErr !== JSON_ERROR_NONE) {
                $jsonErrMsg = json_last_error_msg();
                $response->error--;
                $response->errorMessage = "JSON Error $jsonErr: $jsonErrMsg";

                return $response;
            }

            $response->data = $jsonResponse;
            $response->success = true;
            unset($response->error);
            unset($response->errorMessage);
        }

        return $response;
    }
}
